import { AngularReduxSimplePage } from './app.po';

describe('angular-redux-simple App', () => {
  let page: AngularReduxSimplePage;

  beforeEach(() => {
    page = new AngularReduxSimplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
