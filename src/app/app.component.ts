import { Component, OnDestroy } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store/lib/src';
import { IAppState } from './store';
import { CounterActions } from './counter-actions';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  @select() readonly count$: Observable<number>;
  // count: number;
  // readonly count$: Observable<number>;
  // subscription;

  constructor(private ngRedux: NgRedux<IAppState>, private actions: CounterActions) {
      // this.count$ = ngRedux.select<number>('count');
      //  this.subscription = ngRedux.select<number>('count').subscribe(newCount => this.count = newCount);
  }

  increment() {
    this.ngRedux.dispatch(this.actions.increment());
  }

  decrement() {
    this.ngRedux.dispatch(this.actions.decrement());
  }

  // ngOnDestroy(): void {
  //   this.subscription.unsubscribe(); // <- New
  // }
}
