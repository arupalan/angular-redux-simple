import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { CounterActions } from './counter-actions';
import { IAppState, rootReducer, INITIAL_STATE } from './store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgReduxModule, // <- New
  ],
  providers: [CounterActions],
  bootstrap: [AppComponent]
  })

  export class AppModule {
    constructor(ngRedux: NgRedux<IAppState>,
    devTools: DevToolsExtension) {

      const storeEnhancers = devTools.isEnabled() ? [devTools.enhancer()] : [];
    // Tell @angular-redux/store about our rootReducer and our initial state.
    // It will use this to create a redux store for us and wire up all the
    // events.
    ngRedux.configureStore(
      rootReducer,
      INITIAL_STATE,
      [],
      storeEnhancers);
  }
}
